import * as React from 'react';
import './styles/contacts.scss';

export const Contacts = () => {
    return (
        <div className="contacts">
            <table align="center">
                <tbody>
                    <tr className="contacts__phone">
                        <td>
                            <img
                                className="photo__thumbnail"
                                src={require("../../images/phone-call.png")}
                                alt="telefono numeris"
                            />
                        </td>
                        <td>
                            <a href="tel:+37068530013">+370 685 30013</a>
                        </td>
                    </tr>
                    <tr className="contacts__email">
                        <td>
                            <img
                                className="photo__thumbnail"
                                src={require("../../images/envelope.png")}
                                alt="e-paštas"
                            />
                        </td>
                        <td>
                            <a href="mailto:brokere.aldona@gmail.com">brokere.aldona@gmail.com</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

    );
};