import * as React from 'react';
import '../styles/index.scss';
import {Services} from './Services/Services';
import {Contacts} from "./Contacts/Contacts";
import {Header} from "./Header/Header";
import {Footer} from "./Footer/Footer";

export class App extends React.Component {
    render() {
        return (
            <div>
                <div className="section">
                    <Header className={'container'}/>
                </div>
                <div className="section">
                    <div className="container section__container">
                        <div className="section__title"> 
                            Apie mane
                        </div>
                        <div className="tac">
                            Mano veikla yra susijusi su nekilnojamuoju turtu bei tarpininkavimo pasalugomis.
                            Dažnai domiuosiu nekilnojamojo turto rinka Biržuose ir Biržų rajone, daug bendrauju su žmonėmis
                            norinčiais parduoti nekilnojamąjį turtą tad suprantu jūsų lūkesčius ir pageidavimus.
                            Džiaugiuosi, kad savo hobį paverčiau nuolatiniu užsiėmimu teikiančiu abipusę naudą tiek klientams tiek man.
                        </div>
                    </div>
                </div>
                <div className="section services">
                    <div className="container section__container">
                        <div className="section__title"> 
                            Paslaugos
                        </div>
                        <Services />	
                    </div>
                </div>
                <div className="section section__container">
                    <div className="container">
                        <div className="section__title"> 
                            Kontaktai
                        </div>
                        <Contacts />
                    </div>
                </div>
                <div className="section">
                    <div className="container">
                        <Footer />
                    </div>
                </div>
            </div>
        );
    }
}