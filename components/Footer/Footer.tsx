import * as React from "React";
import './styles/footer.scss';

export const Footer = () => {
    return (
        <div className="footer">
            Icons made by&nbsp;
            <a href="http://www.freepik.com/" title="Freepik">Freepik</a> from&nbsp;
            <a href="https://www.flaticon.com/" title="Flaticon">
                www.flaticon.com
            </a> is licensed by&nbsp;
            <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
        </div>
    );
};