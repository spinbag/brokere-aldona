import * as React from 'react';
import './styles/header.scss';

export const Header = (props: {className?: string}) => {
    return (
        <div className={`${props.className ? props.className : ''} header`}>
            <div className="header__photo photo">
                <img className="photo__portrait" src={require("../../images/aldona.png")} />
            </div>
            <div className="header__text">
                Brokerė Aldona
            </div>
        </div>
    );
};