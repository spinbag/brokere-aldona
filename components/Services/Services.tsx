import * as React from 'react';
import './styles/services.scss';
import {SERVICES} from './services-map';

export interface IService {
    iconUrl: string;
    text: string;
    alt: string;
}

export const Services = () => {
    return (
        <div className='services'>
            {
                SERVICES.map((service, i) => 
                    <div className='services__service services__column' key={i}>
                        <img className="photo__icon" src={service.iconUrl} alt={service.alt} />
                        <div className='services__text'>{service.text}</div>
                    </div>
                )
            }
        </div>

    );
}