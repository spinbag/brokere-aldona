import { IService } from "./Services";

export const SERVICES: Array<IService> = [
    {
        text: 'Analizuoju nekilnojamojo turto rinką. Prileminariai nustatau buto, namo, sklypo bei komercinių patalpų rinkos vertę.',
        iconUrl: require("../../images/line-chart.png"),
        alt: 'Nustatau būsto vertę'
    },
    {
        text: 'Padedu surasti nekilnojamąjį turtą pagal jūsų poreikius.',
        iconUrl: require("../../images/loupe.png"),
        alt: 'Padedu rasti ko ieškote'
    },
    {
        text: 'Nemokamai konsultuoju turto pardavimo, pirkimo ir nuomos klausimais Biržuose ir Biržų rajone.',
        iconUrl: require("../../images/information.png"),
        alt: 'Konsultuoju nemokamai'
    },
    {
        text: 'Objektą nufotografuoju ir patalpinu skelbimus internete. Rūpinuosi skelbimo atnaujinimu bei matomumu.',
        iconUrl: require("../../images/laptop.png"),
        alt: 'Pasiekiu rezultatus naudojantis technologijomis'
    },
    {
        text: 'Padedu tvarkyti turto nuosavybės ir kitus su turto pardavimu pirkimu ir nuoma susijusius dokumentus.',
        iconUrl: require("../../images/file-2.png"),
        alt: 'Padedu tvarkyti dokumentus'
    },
    {
        text: 'Atstovauju kliento interesus sudarant nekilnojamojo turto sandorius.',
        iconUrl: require("../../images/users.png"),
        alt: 'Atstovauju kliento interesus'
    },
    {
        text: 'Dirbu su klientais gyvenančiais užsienyje kuriems reikia brokerio paslaugų Lietuvoje.',
        iconUrl: require("../../images/planet-earth.png"),
        alt: 'Dirbu su klientais iš užsienio'
    }
];